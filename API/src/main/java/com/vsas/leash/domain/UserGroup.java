package com.vsas.leash.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "usergroups")
public class UserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usergroup_id")
    private Long id;

    @Column(name = "uuid", length = 36)
    @NotNull
    @Size(min = 36, max = 36)
    private String uuid;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usergroup_leader", referencedColumnName = "user_id", nullable = false)
    @JsonManagedReference
    private LeashUser groupLeader;

    @Column(name = "maximum_distance")
    private double maximumDistance;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_usersgroups", joinColumns = @JoinColumn(name = "usergroup_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
    @JsonBackReference
    // @JoinTable(name = "users_usersgroups", joinColumns = @JoinColumn(name =
    // "user_id", referencedColumnName = "user_id"), inverseJoinColumns =
    // @JoinColumn(name = "usergroup_id", referencedColumnName = "usergroup_id"))
    private Set<LeashUser> users;

    public UserGroup() {
        super();
    }

    /**
     * @param groupLeader
     * @param maximumDistance
     * @param users
     */
    public UserGroup(@NotNull LeashUser groupLeader, double maximumDistance) {
        super();
        this.groupLeader = groupLeader;
        this.maximumDistance = maximumDistance;
        users = new HashSet<LeashUser>();
        users.add(groupLeader);
        uuid = UUID.randomUUID().toString();
    }

    /**
     * @param groupLeader
     * @param maximumDistance
     * @param users
     */
    public UserGroup(@NotNull LeashUser groupLeader, double maximumDistance, Set<LeashUser> users) {
        super();
        this.groupLeader = groupLeader;
        this.maximumDistance = maximumDistance;
        this.users = users;
        uuid = UUID.randomUUID().toString();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the groupLeader
     */
    public LeashUser getGroupLeader() {
        return groupLeader;
    }

    /**
     * @param groupLeader the groupLeader to set
     */
    public void setGroupLeader(LeashUser groupLeader) {
        this.groupLeader = groupLeader;
    }

    /**
     * @return the minimumDistance
     */
    public double getMaximumDistance() {
        return maximumDistance;
    }

    /**
     * @param minimumDistance the minimumDistance to set
     */
    public void setMaximumDistance(double maximumDistance) {
        this.maximumDistance = maximumDistance;
    }

    /**
     * @return the users
     */
    public Set<LeashUser> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(Set<LeashUser> users) {
        this.users = users;
    }

    /**
     * @return the guid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param guid the guid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
