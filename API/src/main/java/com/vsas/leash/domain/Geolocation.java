package com.vsas.leash.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "geolocations")
public class Geolocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "geolocation_id")
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private LeashUser user;

    @NotNull
    @NotEmpty
    @Column(name = "latitude")
    private String latitude;

    @NotNull
    @NotEmpty
    @Column(name = "longitude")
    private String longitude;

    @NotEmpty
    @Column(name = "time_recorded")
    private Date timeRecorded;

    /**
     *
     */
    public Geolocation() {
	super();
    }

    /**
     * @param user
     * @param latitude
     * @param longitude
     * @param timeRecorded
     */
    public Geolocation(@NotNull LeashUser user, @NotNull @NotEmpty String latitude, @NotNull @NotEmpty String longitude,
	    @NotEmpty Date timeRecorded) {
	super();
	this.user = user;
	this.latitude = latitude;
	this.longitude = longitude;
	this.timeRecorded = timeRecorded;
    }

    /**
     * @param id
     * @param user
     * @param latitude
     * @param longitude
     * @param timeRecorded
     */
    public Geolocation(Long id, @NotNull LeashUser user, @NotNull @NotEmpty String latitude,
	    @NotNull @NotEmpty String longitude, @NotEmpty Date timeRecorded) {
	super();
	this.id = id;
	this.user = user;
	this.latitude = latitude;
	this.longitude = longitude;
	this.timeRecorded = timeRecorded;
    }

    /**
     * @return the id
     */
    public Long getId() {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
	this.id = id;
    }

    /**
     * @return the user
     */
    public LeashUser getUser() {
	return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(LeashUser user) {
	this.user = user;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
	return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
	this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
	return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
	this.longitude = longitude;
    }

    /**
     * @return the timeRecorded
     */
    public Date getTimeRecorded() {
	return timeRecorded;
    }

    /**
     * @param timeRecorded the timeRecorded to set
     */
    public void setTimeRecorded(Date timeRecorded) {
	this.timeRecorded = timeRecorded;
    }

}
