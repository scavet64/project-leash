package com.vsas.leash.domain.exceptions;

public class UserNotFoundException extends DomainObjectNotFound {

    private static final long serialVersionUID = 2780173646792707415L;

    public UserNotFoundException() {
        super("Username was not found");
    }

    public UserNotFoundException(String username) {
        super("Username: " + username + " was not found");
    }
}
