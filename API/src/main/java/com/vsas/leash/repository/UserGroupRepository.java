
package com.vsas.leash.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.UserGroup;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {
    List<UserGroup> findAllByGroupLeader(LeashUser groupLeader);

    Optional<UserGroup> findByUuid(String uuid);
}
