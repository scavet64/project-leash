package com.vsas.leash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsas.leash.domain.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

   Role findByName(String rolename);

}