package com.vsas.leash.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vsas.leash.domain.LeashUser;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<LeashUser, Integer> {

    Optional<LeashUser> findByUsername(String username);
}
