package com.vsas.leash.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.vsas.leash.domain.Geolocation;
import com.vsas.leash.domain.LeashUser;

public interface GeolocationRepository extends JpaRepository<Geolocation, Long> {
    List<Geolocation> findAllByUser(LeashUser userToFind);

    Geolocation findByUserOrderByTimeRecordedDesc(LeashUser user);

    List<Geolocation> findAll(Specification<Geolocation> latestGeolocationsForGroup);
}
