package com.vsas.leash.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.exceptions.UserNotFoundException;
import com.vsas.leash.repository.UserRepository;

@Service
public class UserDetailsMapperService {

    @Autowired
    private UserRepository userRepository;

    public LeashUser getLeashUserFromUserDetails(UserDetails userDetails) throws UserNotFoundException {
	LeashUser user = userRepository.findByUsername(userDetails.getUsername())
		.orElseThrow(() -> new UserNotFoundException(userDetails.getUsername()));

	return user;
    }

}
