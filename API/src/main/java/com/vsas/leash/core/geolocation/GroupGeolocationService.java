package com.vsas.leash.core.geolocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsas.leash.core.geolocation.specification.GeolocationFilterSpecification;
import com.vsas.leash.core.groups.GroupNotFoundException;
import com.vsas.leash.domain.Geolocation;
import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.UserGroup;
import com.vsas.leash.repository.GeolocationRepository;
import com.vsas.leash.repository.UserGroupRepository;
import com.vsas.leash.repository.UserRepository;

@Service
public class GroupGeolocationService {

    @Autowired
    UserRepository userRepos;

    @Autowired
    GeolocationRepository geolocationRepo;

    @Autowired
    UserGroupRepository userGroupRepos;

    @Autowired
    GeolocationFilterSpecification specification;

    public List<Geolocation> getUserGeolocationsLocations(UserGroup userGroup) {
        List<Geolocation> locations = new ArrayList<Geolocation>();

        for (LeashUser user : userGroup.getUsers()) {
            locations.add(geolocationRepo.findByUserOrderByTimeRecordedDesc(user));
        }

        return locations;
    }

    /**
     * Will return a map of group Ids to the geo locations of all users
     * 
     * @param user
     * @return
     */
    public Map<Long, List<Geolocation>> getUserGeolocationsLocations(LeashUser user) {
        Map<Long, List<Geolocation>> groupToUserGeolocations = new HashMap<Long, List<Geolocation>>();

        List<UserGroup> groups = user.getGroups();

        for (UserGroup group : groups) {
            groupToUserGeolocations.put(group.getId(), getUserGeolocationsLocations(group));
        }

        return groupToUserGeolocations;
    }

    /**
     * Will return a map of group Ids to the geo locations of all users
     * 
     * @param user
     * @return
     * @throws GroupNotFoundException
     */
    public List<Geolocation> getUserGeolocationsInGroup(String uuid) throws GroupNotFoundException {
        return geolocationRepo.findAll(specification.getLatestGeolocationsForGroup(uuid));
    }

}
