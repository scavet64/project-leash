package com.vsas.leash.core.groups;

import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.exception.DataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.UserGroup;
import com.vsas.leash.domain.exceptions.DomainObjectNotFound;
import com.vsas.leash.domain.exceptions.UserNotFoundException;
import com.vsas.leash.repository.UserGroupRepository;
import com.vsas.leash.repository.UserRepository;

@Service
@Transactional
public class GroupService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserGroupRepository groupRepo;

    public UserGroup createNewUserGroup(String creatorUsername, CreateGroupRequest req)
            throws UserNotFoundException, GroupCreationException {
        // TODO: Do we care if a user has multiple groups?
        try {
            LeashUser groupLeader = userRepo.findByUsername(creatorUsername)
                    .orElseThrow(() -> new UserNotFoundException(creatorUsername));

            UserGroup newGroup = new UserGroup(groupLeader, req.getMaximumDistance());
            newGroup = groupRepo.save(newGroup);
            return newGroup;
        } catch (NumberFormatException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new GroupCreationException("Could not parse Maximum Distance", ex);
        } catch (DataException ex) {
            throw new GroupCreationException("Could not create new group", ex);
        } catch (UserNotFoundException ex) {
            throw ex;
        }
    }

    public UserGroup addUserToGroup(String username, Long groupId) throws DomainObjectNotFound, GroupAddingException {
        // TODO: Do we care if a user has multiple groups?
        try {
            LeashUser userToAdd = userRepo.findByUsername(username)
                    .orElseThrow(() -> new UserNotFoundException(username));
            UserGroup group = groupRepo.findById(groupId).orElseThrow(() -> new GroupNotFoundException(groupId));

            userToAdd.addUserGroup(group);
            userRepo.save(userToAdd);
            return group;
        } catch (DataException ex) {
            throw new GroupAddingException("Could not create new group", ex);
        } catch (UserNotFoundException | GroupNotFoundException ex) {
            throw ex;
        }
    }

//    public List<UserGroup> getGroupsForUser(String username) throws DomainObjectNotFound, GroupException {
//        // TODO: Do we care if a user has multiple groups?
//        try {
//            LeashUser user = userRepo.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
//            return user.getGroups();
//        } catch (DataException ex) {
//            throw new GroupException("Could not create new group", ex);
//        } catch (UserNotFoundException ex) {
//            throw ex;
//        }
//    }
//
    public Set<LeashUser> getUsersForGroup(Long groupId) throws DomainObjectNotFound, GroupException {
        // TODO: Do we care if a user has multiple groups?
        try {
            UserGroup group = groupRepo.findById(groupId).orElseThrow(() -> new GroupNotFoundException(groupId));
            return group.getUsers();
        } catch (DataException ex) {
            throw new GroupException("Could not create new group", ex);
        } catch (GroupNotFoundException ex) {
            throw ex;
        }
    }

}
