package com.vsas.leash.core.groups;

import static org.springframework.http.ResponseEntity.ok;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vsas.leash.core.response.DataResponse;
import com.vsas.leash.core.response.ErrorResponse;
import com.vsas.leash.core.response.Response;
import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.UserGroup;
import com.vsas.leash.domain.exceptions.DomainObjectNotFound;
import com.vsas.leash.domain.exceptions.UserNotFoundException;

@RestController
@RequestMapping("/group")
public class GroupController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GroupService groupService;

    @PostMapping("/create")
    public ResponseEntity<Response> createNewGroup(@AuthenticationPrincipal UserDetails user,
            @RequestBody CreateGroupRequest req) {
        try {
            UserGroup newGroup = groupService.createNewUserGroup(user.getUsername(), req);

            return ok(new DataResponse(userGroupToReturnMap(newGroup)));
        } catch (GroupCreationException ex) {
            ex.printStackTrace();
            logger.info(ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(ex));
        } catch (UserNotFoundException ex) {
            logger.debug("Could create a new group. Username was not found", ex);
            logger.info(ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex));
        }
    }

    /**
     * TODO: I am thinking this might not end up being used but creating the
     * endpoint for now for completeness.
     * 
     * @param req
     * @return
     */
    @PostMapping("/add")
    public ResponseEntity<Response> addUserToGroup(@AuthenticationPrincipal UserDetails user,
            @RequestBody AddUserToGroupRequest req) {
        try {
            UserGroup userGroup = groupService.addUserToGroup(user.getUsername(), req.getGroupId());
            return ok(new DataResponse(userGroupToReturnMap(userGroup)));
        } catch (GroupAddingException ex) {
            ex.printStackTrace();
            logger.info(ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(ex));
        } catch (DomainObjectNotFound ex) {
            // TODO Auto-generated catch block
            logger.debug("Could not add user to group. Domain object was not found", ex);
            logger.info(ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex));
        }
    }

//    @GetMapping
//    public ResponseEntity<Response> getGroupsForUser(@AuthenticationPrincipal UserDetails user) {
//        try {
//            List<UserGroup> newGroups = groupService.getGroupsForUser(user.getUsername());
//
//            return ok(new DataResponse(newGroups));
//        } catch (DomainObjectNotFound ex) {
//            logger.debug("Could not get user groups", ex);
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(ex));
//        } catch (GroupException ex) {
//            logger.debug("Could not get user groups", ex);
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex));
//        }
//    }
//
    @GetMapping("{id}")
    public ResponseEntity<Response> getUsersInGroup(@PathVariable Long id) {
        try {
            Set<LeashUser> newGroups = groupService.getUsersForGroup(id);

            return ok(new DataResponse(newGroups));
        } catch (DomainObjectNotFound ex) {
            logger.debug("Could not get user groups", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(ex));
        } catch (GroupException ex) {
            logger.debug("Could not get user groups", ex);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex));
        }
    }

    private Map<String, Object> userGroupToReturnMap(UserGroup userGroup) {
        Map<String, Object> returnMap = new HashMap<String, Object>();
        returnMap.put("UUID", userGroup.getUuid());
        returnMap.put("id", userGroup.getId());
        returnMap.put("GroupLeader", userGroup.getGroupLeader().getUsername());
        returnMap.put("maximumDistance", userGroup.getMaximumDistance());
        List<String> users = new ArrayList<String>(userGroup.getUsers().size());
        for (LeashUser user : userGroup.getUsers()) {
            users.add(user.getUsername());
        }
        returnMap.put("usersInGroup", users);

        return returnMap;
    }
}
