package com.vsas.leash.core.groups;

public class AddUserToGroupRequest {

    private Long groupId;

    public AddUserToGroupRequest() {
        super();
    }

    /**
     * @param username The Username of the person to add
     * @param groupId  The Group ID to join
     */
    public AddUserToGroupRequest(Long groupId) {
        super();
        this.groupId = groupId;
    }

    /**
     * @return the groupId
     */
    public Long getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

}
