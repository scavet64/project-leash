package com.vsas.leash.core.groups;

public class CreateGroupRequest {

    private Double maximumDistance;

    /**
     * @param groupLeaderUsername
     * @param maximumDistance
     */
    public CreateGroupRequest() {
        super();
    }

    /**
     * @param groupLeaderUsername
     * @param maximumDistance
     */
    public CreateGroupRequest(Double maximumDistance) {
        super();
        this.maximumDistance = maximumDistance;
    }

    /**
     * @return the maximumDistance
     */
    public Double getMaximumDistance() {
        return maximumDistance;
    }

    /**
     * @param maximumDistance the maximumDistance to set
     */
    public void setMaximumDistance(Double maximumDistance) {
        this.maximumDistance = maximumDistance;
    }

}
