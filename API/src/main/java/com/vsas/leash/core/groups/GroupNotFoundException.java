package com.vsas.leash.core.groups;

import com.vsas.leash.domain.exceptions.DomainObjectNotFound;

public class GroupNotFoundException extends DomainObjectNotFound {

    private static final long serialVersionUID = 9000163118078289642L;

    public GroupNotFoundException() {
        super("Could not find group");
    }

    public GroupNotFoundException(Long id) {
        super("Could not find group with ID: " + id);
    }

    public GroupNotFoundException(String message) {
        super(message);
    }

    public GroupNotFoundException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public GroupNotFoundException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public GroupNotFoundException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

}
