package com.vsas.leash.core.groups;

public class GroupException extends Exception {

    public GroupException() {
        // TODO Auto-generated constructor stub
    }

    public GroupException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public GroupException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public GroupException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public GroupException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

}
