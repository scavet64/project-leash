package com.vsas.leash.core.geolocation;

import static org.springframework.http.ResponseEntity.ok;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.exceptions.UserNotFoundException;
import com.vsas.leash.security.services.UserDetailsMapperService;

@RestController
@RequestMapping("/geolocation/user")
public class UserGeolocationController {

    @Autowired
    UserGeolocationService userLocationService;

    @Autowired
    UserDetailsMapperService userDetailsMapperService;

    @GetMapping("/update")
    public ResponseEntity<String> currentUser(@AuthenticationPrincipal UserDetails userDetails,
	    GeolocationUpdateRequest updateRequest) {
	try {
	    LeashUser userToUpdate = userDetailsMapperService.getLeashUserFromUserDetails(userDetails);
	    userLocationService.saveUserLocation(userToUpdate, updateRequest);
	} catch (UserNotFoundException ex) {
	    return ResponseEntity.badRequest().body(ex.getLocalizedMessage());
	} catch (GeolocationUpdateException e) {
	    e.printStackTrace();
	} catch (ParseException ex) {
	    return ResponseEntity.badRequest().body(ex.getLocalizedMessage());
	}

	return ok("");
    }
}
