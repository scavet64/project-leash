package com.vsas.leash.core.geolocation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GeolocationUpdateRequest {
    private String latitude;
    private String longitude;
    private String timeRecorded;

    /**
     *
     */
    public GeolocationUpdateRequest() {
	super();
    }

    /**
     * @param latitude
     * @param longitude
     * @param timeRecorded
     */
    public GeolocationUpdateRequest(String latitude, String longitude, String timeRecorded) {
	super();
	this.latitude = latitude;
	this.longitude = longitude;
	this.timeRecorded = timeRecorded;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
	return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
	this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
	return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
	this.longitude = longitude;
    }

    /**
     * @return the timeRecorded
     */
    public String getTimeRecorded() {
	return timeRecorded;
    }

    /**
     * @param timeRecorded the timeRecorded to set
     */
    public void setTimeRecorded(String timeRecorded) {
	this.timeRecorded = timeRecorded;
    }

    public Date getDateRecorded() throws ParseException {
	SimpleDateFormat formatter6 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	return formatter6.parse(timeRecorded);
    }

}
