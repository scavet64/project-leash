package com.vsas.leash.core.users.managment;

import static org.springframework.http.ResponseEntity.ok;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vsas.leash.core.response.DataResponse;
import com.vsas.leash.core.response.ErrorResponse;
import com.vsas.leash.core.response.Response;
import com.vsas.leash.domain.exceptions.DomainObjectNotFound;
import com.vsas.leash.domain.exceptions.UserNotFoundException;

@RestController
@RequestMapping("/users")
public class UserManagamentRestController {

    @Autowired
    private UserManagementService ums;

    /**
     * Returns the information for the currently logged in user
     * 
     * @param userDetails the user details for the currently logged in user. This is
     *                    injected by spring
     * @return A response entity to send back to the caller
     * @throws UserNotFoundException
     */
    @GetMapping("/me")
    public ResponseEntity<Response> currentUser(@AuthenticationPrincipal UserDetails userDetails) {
	try {
	    return ok(new DataResponse(ums.getUserInformation(userDetails)));
	} catch (DomainObjectNotFound e) {
	    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse(e));
	}
    }

}
