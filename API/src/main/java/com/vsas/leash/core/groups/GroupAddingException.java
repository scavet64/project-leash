package com.vsas.leash.core.groups;

public class GroupAddingException extends GroupException {

    private static final long serialVersionUID = 6272044511466321439L;

    public GroupAddingException() {
        // TODO Auto-generated constructor stub
    }

    public GroupAddingException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public GroupAddingException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public GroupAddingException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public GroupAddingException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

}
