package com.vsas.leash.core.groups;

public class GroupCreationException extends GroupException {
    private static final long serialVersionUID = 4522488456673083062L;

    public GroupCreationException() {
        // TODO Auto-generated constructor stub
    }

    public GroupCreationException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public GroupCreationException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public GroupCreationException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public GroupCreationException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

}
