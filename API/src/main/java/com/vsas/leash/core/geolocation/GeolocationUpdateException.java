/**
 * 
 */
package com.vsas.leash.core.geolocation;

/**
 * @author Vincent
 *
 */
public class GeolocationUpdateException extends Exception {

    private static final long serialVersionUID = -5190247864442287958L;

    /**
     * 
     */
    public GeolocationUpdateException() {
	// TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public GeolocationUpdateException(String message) {
	super(message);
	// TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public GeolocationUpdateException(Throwable cause) {
	super(cause);
	// TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public GeolocationUpdateException(String message, Throwable cause) {
	super(message, cause);
	// TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public GeolocationUpdateException(String message, Throwable cause, boolean enableSuppression,
	    boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
	// TODO Auto-generated constructor stub
    }

}
