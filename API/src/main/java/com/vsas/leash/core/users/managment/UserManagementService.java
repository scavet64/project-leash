package com.vsas.leash.core.users.managment;

import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.vsas.leash.domain.exceptions.UserNotFoundException;
import com.vsas.leash.repository.UserRepository;
import com.vsas.leash.security.services.UserDetailsMapperService;

@Service
public class UserManagementService {

    @Autowired
    private UserRepository users;

    @Autowired
    private UserDetailsMapperService mapper;

    @Transactional
    public Map<Object, Object> getUserInformation(UserDetails userDetails) throws UserNotFoundException {
	Map<Object, Object> model = new HashMap<>();
	Map<Object, Object> secInfo = new HashMap<>();
	secInfo.put("Username", userDetails.getUsername());
	secInfo.put("Authorities", userDetails.getAuthorities().stream().map(a -> ((GrantedAuthority) a).getAuthority())
		.collect(toList()));

	model.put("Security Information", secInfo);
	model.put("User Object", users.findByUsername(userDetails.getUsername()));
	model.put("User Object", mapper.getLeashUserFromUserDetails(userDetails));
	return model;
    }

}
