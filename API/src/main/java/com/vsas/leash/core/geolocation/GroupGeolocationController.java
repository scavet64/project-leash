package com.vsas.leash.core.geolocation;

import static org.springframework.http.ResponseEntity.ok;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vsas.leash.core.groups.GroupNotFoundException;
import com.vsas.leash.core.response.DataResponse;
import com.vsas.leash.core.response.ErrorResponse;
import com.vsas.leash.core.response.Response;
import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.exceptions.UserNotFoundException;
import com.vsas.leash.security.services.UserDetailsMapperService;

@RestController
@RequestMapping("/geolocation/group")
public class GroupGeolocationController {

    @Autowired
    GroupGeolocationService geolocationService;

    @Autowired
    UserDetailsMapperService userDetailsMapperService;

    @GetMapping
    public ResponseEntity<String> currentUser(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            LeashUser requestingUser = userDetailsMapperService.getLeashUserFromUserDetails(userDetails);
            geolocationService.getUserGeolocationsLocations(requestingUser);
        } catch (UserNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getLocalizedMessage());
        }

        return ok("");
    }

    @GetMapping("{uuid}")
    public ResponseEntity<Response> getGeolocationsForGroup(@AuthenticationPrincipal UserDetails userDetails,
            @PathVariable String uuid) {
        try {
            return ResponseEntity.ok(new DataResponse(geolocationService.getUserGeolocationsInGroup(uuid)));
        } catch (GroupNotFoundException ex) {
            return ResponseEntity.badRequest().body(new ErrorResponse(ex.getLocalizedMessage()));
        }
    }

}
