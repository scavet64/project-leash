package com.vsas.leash.core.users.registration;

import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.security.jwt.JsonWebToken;

public class RegistrationResponse {

    private LeashUser newUser;
    private JsonWebToken token;

    /**
     * @param newUser
     * @param token
     */
    public RegistrationResponse(LeashUser newUser, JsonWebToken token) {
	super();
	this.newUser = newUser;
	this.token = token;
    }

    /**
     * @return the newUser
     */
    public LeashUser getNewUser() {
	return newUser;
    }

    /**
     * @param newUser the newUser to set
     */
    public void setNewUser(LeashUser newUser) {
	this.newUser = newUser;
    }

    /**
     * @return the token
     */
    public JsonWebToken getToken() {
	return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(JsonWebToken token) {
	this.token = token;
    }

}
