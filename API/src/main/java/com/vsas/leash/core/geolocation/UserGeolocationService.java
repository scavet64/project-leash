package com.vsas.leash.core.geolocation;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vsas.leash.domain.Geolocation;
import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.repository.GeolocationRepository;

@Service
public class UserGeolocationService {

    @Autowired
    private GeolocationRepository geolocationRepo;

    public void saveUserLocation(LeashUser user, GeolocationUpdateRequest geoLocation)
            throws GeolocationUpdateException, ParseException {
        try {
            Geolocation userLocation = new Geolocation(user, geoLocation.getLatitude(), geoLocation.getLongitude(),
                    geoLocation.getDateRecorded());
            geolocationRepo.save(userLocation);
        } catch (ParseException parseEx) {
            throw parseEx;
        }
    }

}
