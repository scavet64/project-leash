package com.vsas.leash.core.geolocation.specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.vsas.leash.domain.Geolocation;
import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.UserGroup;
import com.vsas.leash.specifications.FilterSpecification;

@Service
public class GeolocationFilterSpecification extends FilterSpecification<Geolocation> {

    public Specification<Geolocation> getLatestGeolocationsForGroup(String uuid) {
        return (root, query, cb) -> {
            // building the desired query
            root.fetch("user", JoinType.LEFT);
            Join<Geolocation, LeashUser> user = root.join("user", JoinType.INNER);
            Join<LeashUser, UserGroup> group = user.join("groups", JoinType.INNER);
            query.groupBy(user.get("id"));
            query.orderBy(cb.desc(root.get("timeRecorded")));
            return cb.equal(group.get("uuid"), uuid);
        };
    }
}