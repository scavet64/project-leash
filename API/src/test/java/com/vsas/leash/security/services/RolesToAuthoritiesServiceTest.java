package com.vsas.leash.security.services;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.vsas.leash.domain.Privilege;
import com.vsas.leash.domain.Role;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RolesToAuthoritiesServiceTest {

    @Autowired
    RolesToAuthoritiesService rolesToAuthoritiesService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getAuthorities_adminRole() {
	ArrayList<Role> roles = new ArrayList<Role>();
	Role adminRole = new Role("Admin");
	adminRole.getPrivileges().add(new Privilege("read"));
	adminRole.getPrivileges().add(new Privilege("write"));
	roles.add(adminRole);
	Collection<?> authorities = rolesToAuthoritiesService.getAuthorities(roles);
	assertEquals(3, authorities.size());
    }

    @Test
    public void getAuthorities_emptyRole() {
	ArrayList<Role> roles = new ArrayList<Role>();
	Collection<?> authorities = rolesToAuthoritiesService.getAuthorities(roles);
	assertEquals(0, authorities.size());
    }

    @Test
    public void getAuthorities_manyRoles() {
	ArrayList<Role> roles = new ArrayList<Role>();
	Role adminRole = new Role("Admin");
	adminRole.getPrivileges().add(new Privilege("read"));
	adminRole.getPrivileges().add(new Privilege("write"));
	roles.add(adminRole);
	roles.add(adminRole);
	roles.add(adminRole);
	roles.add(adminRole);
	Collection<?> authorities = rolesToAuthoritiesService.getAuthorities(roles);
	assertEquals(12, authorities.size());
    }

}
