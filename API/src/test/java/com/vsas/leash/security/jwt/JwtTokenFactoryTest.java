package com.vsas.leash.security.jwt;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.vsas.leash.domain.LeashUser;
import com.vsas.leash.domain.Role;
import com.vsas.leash.security.services.RolesToAuthoritiesService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtTokenFactoryTest {

    @Autowired
    JwtUserFactory factory;

    @MockBean
    private RolesToAuthoritiesService converter;

    @Before
    public void setup() {
	converter = mock(RolesToAuthoritiesService.class);
    }

    @Test
    public void create_NullUser() {
	try {
	    @SuppressWarnings("unused")
	    JwtUser jwtUser = factory.create(null);
	    fail("Exception should be thrown");
	} catch (IllegalArgumentException ex) {
	    return;
	}

    }

    @Test
    public void create_GoodUser() {
	@SuppressWarnings("unchecked")
	ArrayList<Role> roles = mock(ArrayList.class);
	when(roles.get(anyInt())).thenReturn(new Role("Test"));

	LeashUser mockUser = mock(LeashUser.class);
	when(mockUser.getUsername()).thenReturn("Admin");
	when(mockUser.getPassword()).thenReturn("password");
	when(mockUser.getRoles()).thenReturn(roles);
	// when(converter.getAuthorities(any())).thenReturn(any());
	JwtUser jwtUser = factory.create(mockUser);

	assertNotNull(jwtUser);
    }

}
