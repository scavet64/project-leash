package com.vsas.leash;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.vsas.leash.core.users.managment.UserManagamentRestController;
import com.vsas.leash.core.users.managment.UserManagementService;
import com.vsas.leash.security.jwt.JwtAuthenticationEntryPoint;
import com.vsas.leash.security.jwt.JwtTokenUtil;

@RunWith(SpringRunner.class)
//@SpringBootTest
@AutoConfigureMockMvc
@WebMvcTest(controllers = UserManagamentRestController.class)
public class UserManagamentRestControllerTest {

    @MockBean
    private JwtAuthenticationEntryPoint unauthorizedHandler;
    @MockBean
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailService;
    @MockBean
    private JwtTokenUtil jtw;

    @MockBean
    private UserManagementService ums;

    @Autowired
    private MockMvc mvc;

    @Before
    public void setup() {
        ums = mock(UserManagementService.class);
    }

    @WithMockUser(authorities = "ROLE_USER", username = "admin")
    @Test
    public void getNonSecurePingAsUser() throws Exception {
        when(ums.getUserInformation(any())).thenReturn(new HashMap<Object, Object>());

        mvc.perform(MockMvcRequestBuilders.get("/users/me").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

//	@WithMockUser(authorities = "ROLE_ADMIN")
//	@Test
//	public void getNonSecurePingAsAdmin() throws Exception {
//		mvc.perform(MockMvcRequestBuilders.get("/ping").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
//	}
//
//	@WithMockUser(authorities = "ROLE_USER")
//	@Test
//	public void getPingAdminWithUserAccount() throws Exception {
//		mvc.perform(MockMvcRequestBuilders.get("/ping/admin").accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isForbidden());
//	}
//
//	@WithMockUser(authorities = "ROLE_ADMIN")
//	@Test
//	public void getPingAdminWithAdminAccount() throws Exception {
//		mvc.perform(MockMvcRequestBuilders.get("/ping/admin").accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//	}
}