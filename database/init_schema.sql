DROP DATABASE IF EXISTS leash;
DROP USER IF EXISTS 'leash'@'localhost';
CREATE DATABASE leash;
CREATE USER 'leash'@'localhost' IDENTIFIED BY 'LeashPassword';
GRANT ALL PRIVILEGES ON leash.* TO 'leash'@'localhost';
USE leash;

CREATE TABLE users (
	user_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	username varchar(255) not null,
    firstname varchar(255) not null,
    lastname varchar(255) not null,
    email varchar(255) not null,
	password varchar(255) NOT NULL,
	enabled tinyint(1) default 1,
	token_expired tinyint(1) default 1,
    last_password_reset TIMESTAMP default now(),
	unique key(username)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE users_friends (
	user_id INT NOT NULL,
	friend_id INT NOT NULL,
	foreign key (user_id) references users(user_id),
	foreign key (friend_id) references users(user_id),
    PRIMARY KEY (user_id, friend_id)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE invites (
	invite_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	invitee_id INT NOT NULL,
    group_id INT NOT NULL,
    created_by INT NOT NULL,
	created_date TIMESTAMP NOT NULL,
    last_modified_date TIMESTAMP NOT NULL,
    last_modified_by INT NOT NULL,
    foreign key (invitee_id) references users(user_id),
    foreign key (created_by) references users(user_id),
    foreign key (last_modified_by) references users(user_id)
) Engine=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE roles (
	role_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	role_name varchar(255) not null,
	unique key(role_name)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE privilege (
	privilege_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	privilege_name varchar(255) not null,
	unique key(privilege_name)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE users_roles (
	user_id INT NOT NULL,
	role_id INT NOT NULL,
	foreign key (user_id) references users(user_id),
	foreign key (role_id) references roles(role_id),
    PRIMARY KEY (user_id, role_id)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE roles_privileges (
	role_id INT NOT NULL,
	privilege_id INT NOT NULL,
	foreign key (role_id) references roles(role_id),
	foreign key (privilege_id) references privilege(privilege_id),
    PRIMARY KEY (role_id, privilege_id)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usergroups (
	usergroup_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    uuid varchar(36) NOT NULL UNIQUE,
	usergroup_leader INT not null,
    maximum_distance DOUBLE not null,
    foreign key (usergroup_leader) references users(user_id)
    
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE users_usersgroups (
	user_id INT NOT NULL,
	usergroup_id INT NOT NULL,
	foreign key (user_id) references users(user_id),
	foreign key (usergroup_id) references usergroups(usergroup_id),
    PRIMARY KEY (user_id, usergroup_id)
) Engine=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE geolocations (
	geolocation_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
	latitude varchar(255) not null,
    longitude varchar(255) not null,
    time_recorded timestamp not null,
    foreign key (user_id) references users(user_id)
) Engine=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO roles(role_id, role_name) VALUES(1, "ROLE_ADMIN");
INSERT INTO roles(role_id, role_name) VALUES(2, "ROLE_USER");

INSERT INTO privilege(privilege_id, privilege_name) VALUES(1, "READ_PRIVILEGE");
INSERT INTO privilege(privilege_id, privilege_name) VALUES(2, "WRITE_PRIVILEGE");

INSERT INTO roles_privileges(role_id, privilege_id) VALUES(1, 1);
INSERT INTO roles_privileges(role_id, privilege_id) VALUES(1, 2);
INSERT INTO roles_privileges(role_id, privilege_id) VALUES(2, 1);

INSERT INTO users(user_id, username, firstname, lastname, email, password) VALUES(1, "TESTUSER", "TEST", "USER", "TEST@EMAIL.com", "PASSWORD");
INSERT INTO users(user_id, username, firstname, lastname, email, password) VALUES(2, "TESTUSER2", "TEST", "USER", "TEST2@EMAIL.com", "PASSWORD");
INSERT INTO users(user_id, username, firstname, lastname, email, password) VALUES(3, "TESTUSER3", "TEST", "USER", "TEST3@EMAIL.com", "PASSWORD");
INSERT INTO users(user_id, username, firstname, lastname, email, password) VALUES(4, "TESTUSER4", "TEST", "USER", "TEST4@EMAIL.com", "PASSWORD");
INSERT INTO users(user_id, username, firstname, lastname, email, password) VALUES(5, "TESTUSER5", "TEST", "USER", "TEST5@EMAIL.com", "PASSWORD");

INSERT INTO usergroups(usergroup_id, uuid, usergroup_leader, maximum_distance) VALUES(1, "a944ca71-86d3-4c32-82f9-67115946d5e1", 1, 30);

INSERT INTO users_usersgroups(usergroup_id, user_id) VALUES(1, 1);
INSERT INTO users_usersgroups(usergroup_id, user_id) VALUES(1, 2);
INSERT INTO users_usersgroups(usergroup_id, user_id) VALUES(1, 3);
INSERT INTO users_usersgroups(usergroup_id, user_id) VALUES(1, 4);
INSERT INTO users_usersgroups(usergroup_id, user_id) VALUES(1, 5);

INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(1, 1, "40.712776", "-74.005974", now());
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(2, 1, "40.712716", "-74.005974", now()-100);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(3, 1, "40.712726", "-74.005974", now()-200);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(4, 1, "40.712736", "-74.005974", now()-300);

INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(5, 2, "40.712775", "-74.005975", now());
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(6, 2, "40.712715", "-74.005975", now()-100);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(7, 2, "40.712725", "-74.005975", now()-200);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(8, 2, "40.712725", "-74.005975", now()-300);

INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(9, 3, "40.712774", "-74.005976", now());
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(10, 3, "40.712714", "-74.005976", now()-100);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(11, 3, "40.712724", "-74.005976", now()-200);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(12, 3, "40.712734", "-74.005976", now()-300);

INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(13, 4, "40.712773", "-74.005977", now());
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(14, 4, "40.712713", "-74.005977", now()-100);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(15, 4, "40.712723", "-74.005977", now()-200);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(16, 4, "40.712733", "-74.005977", now()-300);

INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(17, 5, "40.712772", "-74.005978", now());
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(18, 5, "40.712712", "-74.005978", now()-100);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(19, 5, "40.712722", "-74.005978", now()-200);
INSERT INTO geolocations (geolocation_id, user_id, latitude, longitude, time_recorded) VALUES(20, 5, "40.712732", "-74.005978", now()-300);
